package com.deba.deba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class DebaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DebaApplication.class, args);
	}

}
